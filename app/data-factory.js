app.factory('appData', function() {
 var courseSelected = {}
 function set(data) {
   courseSelected = data;
 }
 function get() {
  return courseSelected;
 }

 return {
  set: set,
  get: get
 }

});
