myStore.directive('coursedir', function () {
    return {
        scope: true,
        replace: true,
        templateUrl: '../../templates/story-template.html',
        link: function (scope, elem, attrs) {
            elem.on('click', function () {
                elem.siblings().removeClass('currentStory');
                elem.addClass('currentStory');
            });

            elem.on('mouseover', function () {
                elem.siblings().removeClass('courseFocus');
                elem.addClass('courseFocus');
            });
        }
    }
});