'use strict';
//(function () {
    var myStore = angular.module('qaStoreApp', []);

    myStore.controller('MainCtrl', function ($scope) {

      $scope.title = "QA Training";
      $scope.links = [
        {
            label: "Home",
            href: "index.html",
            tip: "Home page"
        },
        {
              label: "QA Store",
              href: "store-template.html",
              tip: "Visit our store"
        },
        {
              label: "Blog",
              href: "blog.html",
              tip: "Read our blog"
        },
        {
              label: "Course",
              href: "course.html",
              tip: "Attend a course"
        },
        {
            label: "Current News",
            href: "sidebar-template.html",
            tip: "Current News"
        },
        {
            label: "Users",
            href: "user.html",
            tip: "Users"
        },
  ]

        $scope.openPopup = function ($event) {
                console.log('hit');
             //   alert('hit');
            //$event.preventDefault();
        };

    });

    myStore.controller('HeroCtrl', function ($scope) {

        $scope.heroImg = {
            imgSrc: "img/high-tech.png",
            alt: "Really hightech"
        };

    });

    /*
    Story Editor
    */
/*
    myStore.controller('StoryControl', function ($scope) {
        console.dir($scope);
        $scope.feed = [
                    { code: "QAWEBUI", title: "HTML5", description: "Modern Web Apps" , courseType: "Blended"},
                    { code: "QAWEBCSS", title: "Responsive Web", description: "Mobile ready websites", courseType: "Classroom" },
                    { code: "QAUX", title: "UX Foundations", description: "UX Analysis and research", courseType: "Virtual" },
                    { code: "QARAILS", title: "Ruby on Rails", description: "Scaleable web based applications", courseType: "Classroom" },
                    { code: "QASWP", title: "MEAN Stack", description: "Lets get mean!", courseType: "Classroom"},
                    { code: "QASWIFT", title: "iOS using Swift", description: "Develop apps using the Swift programming language", courseType: "Video on Demand" },

        ];


        $scope.courseTypes = [
            { name: "Classroom" },
            { name: "Blended" },
            { name: "Virtual" },
            { name: "Video on Demand" },
        ];



        $scope.setCurrentStory = function (story) {
            $scope.currentStory = story;
            console.dir(story);
        }


    });


//})();
*/


/*'use strict';
(function(){
var myStore = angular.module('qaStoreApp', []);

myStore.controller('MainCtrl', function ($scope) {

    console.dir($scope);
    //$scope.header = myData;
    this.header = myData;
 console.dir(this);

});

var myData = {
    title: "QA Web Store",
    links: [
        {
            label: "QA Store",
            href: "store.html",
            tip: "Visit our store"
        },
        {
            label: "Blog",
            href: "blog.html",
            tip: "Read our blog"
        },
        {
            label: "Course",
            href: "course.html",
            tip: "Attend a course"
        },
    ]
};

        })();*/
/*
angular
  .module('qaStoreApp', [
    'ngCookies',
    'ngResource',
    'ngSanitize',
    'ngRoute'
  ])
  .config(function ($routeProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'views/main.html',
        controller: 'MainCtrl'
      })
      .otherwise({
        redirectTo: '/'
      });
  });
<<<<<<< HEAD

  */



  myStore.controller("userCtrl", function($scope) {
     $scope.users = [
       { username: "John",
         email: "john@here.com",
         role: "visitor"},
       { username: "Paul",
         email: "paul@here.com",
         role: "visitor"},
       { username: "Jim",
         email: "jim@here.com",
         role: "visitor"}
     ];

     $scope.addUser = function() {
       console.log ($scope.user.name);
       console.log ($scope.user.email);
       var newUser = {};
       newUser.username = $scope.user.name;
       newUser.email = $scope.user.email;
       newUser.role = "visitor";
       $scope.users.push(newUser);
       $scope.user.name = null;
       $scope.user.email = null;
       localStorage.removeItem(newUser.email + ".email");
       localStorage.removeItem(newUser.email + ".name");
       localStorage.removeItem(newUser.email + ".role");
       localStorage.setItem(newUser.email+".email",newUser.email);
       localStorage.setItem(newUser.email+".name",newUser.username);
       localStorage.setItem(newUser.email+".role",newUser.role);
     };


     $scope.findUser = function() {
       var thisUser = {};
       var k= $scope.user.email;

       $scope.user.name=  localStorage.getItem(k + ".name");
       $scope.user.role = localStorage.getItem(k + ".role");
       console.dir(thisUser);
     };

     $scope.updateUser = function() {
       localStorage.removeItem($scope.user.email + ".email");
       localStorage.removeItem($scope.user.email + ".name");
       localStorage.removeItem($scope.user.email + ".role");
       localStorage.setItem($scope.user.email+".email",$scope.user.email);
       localStorage.setItem($scope.user.email+".name",$scope.user.name);
       localStorage.setItem($scope.user.email+".role",$scope.user.role);

     }
   });
