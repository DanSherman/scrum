angular.module('qaStoreApp')
  .controller('servicesCtrl', function ($scope) {

      $scope.services = [
          {imgSrc:"img/training.jpg", description:"Training Services",link:"?ID=TS"},
          {imgSrc:"img/coaching.jpg", description:"Coaching Services",link:"?ID=CS"},
          {imgSrc:"img/mentoring.jpg", description:"Mentoring Services",link:"?ID=MS"},
          {imgSrc:"img/consultancy.jpg", description:"Consultancy Services",link:"?ID=SS"}
      ];
  });
