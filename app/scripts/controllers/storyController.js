﻿myStore.controller('StoryControl', function ($scope) {
     console.dir($scope);
        $scope.feed = [
                    { code: "QAWEBUI", title: "HTML5", description: "Modern Web Apps" , courseType: "Blended"},
                    { code: "QAWEBCSS", title: "Responsive Web", description: "Mobile ready websites", courseType: "Classroom" },
                    { code: "QAUX", title: "UX Foundations", description: "UX Analysis and research", courseType: "Virtual" },
                    { code: "QARAILS", title: "Ruby on Rails", description: "Scaleable web based applications", courseType: "Classroom" },
                    { code: "QASWP", title: "MEAN Stack", description: "Lets get mean!", courseType: "Classroom"},
                    { code: "QASWIFT", title: "iOS using Swift", description: "Develop apps using the Swift programming language", courseType: "Video on Demand" },

        ];


        $scope.courseTypes = [
            { name: "Classroom" },
            { name: "Blended" },
            { name: "Virtual" },
            { name: "Video on Demand" },
        ];

        


        $scope.setCurrentStory = function (story) {
            $scope.currentStory = story;
            console.dir(story);
        }


});