
var myStore = angular.module('Courseware', ['ui.grid']);


myStore.factory('CoursewareHelper', function () {
    var buildIndex = function (source, property) {
        var tempArray = [];

        for (var i = 0, len = source.length; i < len; ++i) {
            tempArray[source[i][property]] = source[i];
        }

        return tempArray;
    };

    return {
        buildIndex: buildIndex
    };
});

//part 2 - build service
myStore.service('CorsewareModel', function () {


    var service = this;

    var statuses = [
            { name: 'Back Log' },
            { name: 'To Do' },
            { name: 'Peer Review' },
            { name: 'Verified' },
            { name: 'Done' }
        ];

        var types = [
            { name: 'Classroom' },
            { name: 'Blended' },
            { name: 'Virtual' },
            { name: 'Video on Demand' }
        ];

        var courses = [
            {
                code: 'QAWEBUI',
                title: 'HTML5',
                description: 'Modern Web Apps',
                type: 'Blended',
                status: 'Done',
                author: 'David Walker',
                subject: 'Training Services'
            },
            {
                code: 'QAHOTT',
                title: 'HOTT',
                description: 'Stuff from a to b',
                type: 'Blended',
                status: 'Done',
                author: 'Barry',
                subject: 'Mentoring Services'
            },
            {
                code: 'QAPIG',
                title: 'PIGASUS',
                description: 'All about PIGASUS',
                type: 'Blended',
                status: 'Done',
                author: 'Dan',
                subject: 'Consultancy Services'
            },
            {
                code: 'QAWEBCSS',
                title: 'Responsive Web Apps',
                description: 'Mobile Ready Web Apps',
                type: 'Classroom',
                status: 'Done',
                author: 'David Walker',
                subject: 'Training Services'
            },
            {
                code: 'QAUX',
                title: 'User Experience Fundamentals',
                description: 'Modern Web Apps',
                type: 'Classroom',
                status: 'Back Log',
                author: 'Edsel Tham',
                subject: 'Coaching Services'
            },
            {
                code: 'QAANGULARJS',
                title: 'Building Web Apps using AngularJS',
                description: 'Single web page apps using Angular ',
                type: 'Virtual',
                status: 'Peer Review',
                author: 'Edsel Tham',
                subject: 'Training Services'
            }
        ];

    service.getStatuses = function () {
        return statuses;
    };

    service.getTypes = function () {
        return types;
    };

    service.getCourses = function () {
        console.log(courses);
        return courses;
    };
});

//part 3 - build controller
myStore.controller('MainXCtrl', function (CorsewareModel, CoursewareHelper) {
    //part 4 - build properties
    var main = this;

    main.types = CorsewareModel.getTypes();
    main.statuses = CorsewareModel.getStatuses();
    main.courses = CorsewareModel.getCourses();
    main.typesIndex = CoursewareHelper.buildIndex(main.types, 'name');
    main.statusesIndex = CoursewareHelper.buildIndex(main.statuses, 'name');
    //part 5 - setup course link
    main.setCurrentCourse = function (course) {
        console.log('hit');
        main.currentCourse = course;
        main.currentStatus = main.statusesIndex[course.status];
        console.dir(main.currentStatus);
        main.currentType = main.typesIndex[course.type];
    };

    main.gridOptions={
        enableSorting: true,
        data:main.courses,
        columnDefs: [
        // {field : 'code'}
        {field : 'code'},
        {field : 'title'},
        {field : 'author'},
        {field : 'description'}
      ],
      enableHorizontalScrollbar:0,
      enableVerticalScrollbar:0
    };


    //part 6 - setup dropdown lists
    main.setCurrentStatus = function (status) {
        if (typeof main.currentCourse !== 'undefined') {
            main.currentCourse.status = status.name;
        }
    };


    main.setCurrentType = function (type) {
        if (typeof main.currentCourse !== 'undefined') {
            main.currentCourse.type = type.name;
        }
    };

        //part6 - create new course
    main.createCourse = function () {


        main.courses.push({
            code: 'QACOURSE',
            title: 'Title pending',
            description: 'description pending',
            types: 'Classroom',
            status: 'Back Log',
            author: 'Pending'
        });
        console.log(main.courses);
    };


});
